## Scripts to automate the SHINE installation at CERN/GSI

### Installation on GSI

Firstly, to configure directory structure and download all required packages, use

`
$ ssh <YOUR-GSI-USERNAME>@lxpool.gsi.de
$ ./ConfigureShineGsi.sh` 
on _lxpool_ (since kronos has no access to the Web)

Edit this file if you need specific locations for sources/builds/binaries. By default it will use
the following paths

`
/u/$USER/shape
/u/$USER/shine
/lustre/nyx/cbm/users/$USER/shape_install
/lustre/nyx/cbm/users/$USER/shine_install`


It may ask your lxplus credentials to clone sources from CERN git/svn services

Then simply run on kronos

`
$ ssh <YOUR-GSI-USERNAME>@kronos.hpc
$ ./ShineInstallGsi.sh`

### Runnung Shine


`
$ source ./SetGeant4Var.sh
$ eval $(/lustre/nyx/cbm/users/$USER/shine_install/bin/shine-offline-config --env-sh)
$ ShineOffline
`

## Troubleshooting

### FindGeant4.cmake: No such file or directory

Sometimes Geant4 building fails without any exception. In that case _ShineInstallGsi.sh_ will fail on shine
cmake configuration stage


>CMake Error at CMakeModules/PackagesSearch.cmake:65 (FIND_PACKAGE):
>Could not find module FindGeant4.cmake or a configuration file for package
>Geant4.
>
>Adjust CMAKE_MODULE_PATH to find FindGeant4.cmake or set Geant4_DIR to the
>directory containing a CMake configuration file for Geant4. The file will
>have one of the following names:
>
>Geant4Config.cmake
>
>geant4-config.cmake


It happens due to unpacked sources of geant4 have wrong permissions (-r--r--r--).
To fix it one need to

`find /u/$USER/shape/shape_build/geant4/geant4.9.6.p02/examples -type f -exec chmod 0770 '{}' ';'`

and then rerun installation script

##  Crash on _reform_ client during reconstruction

@see https://twiki.cern.ch/twiki/bin/view/NA61/KnownProblems

Remove it from reconstruction chain (WrappedLegacyReconstruction/ModuleSequence.xml).

# TODO
download all externals with wget, e. g.
wget http://geant4.cern.ch/support/source/geant4.9.6.p02.tar.gz
