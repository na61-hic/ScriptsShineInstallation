#!/bin/bash

LEGACY=$1

bit=64
prodprefix=OFF
isgeant4=ON

if [ "$LEGACY" == "LEGACY" ]; then
    bit=32
    prodprefix=ON
fi

echo Legacy support: $prodprefix

eval $(./ConfigureShineGsi.sh sh)

echo "SHAPE installation directiories"
echo " **** " $shine_dir " **** "
echo "src:      " $shape_src
echo "build:    " $shape_build
echo "install:  " $shape_install
echo "    "

echo "SHINE installation directiories"
echo " **** " $shine_dir " **** "
echo "src:      " $shine_src
echo "build:    " $shine_build
echo "install:  " $shine_install
echo "db        " $db_dir
echo "    "

echo "SHINE sources will be copied from:"
echo $shape_src_svn
echo "SHAPE sources will be copied from:"
echo $shine_src_gitclone

mkdir -p $shine_dir
mkdir -p $shape_dir

mkdir -p $shape_install
mkdir -p $shape_build
mkdir -p $shape_src

mkdir -p $shine_build
mkdir -p $shine_install
mkdir -p $shine_src

mkdir -p $db_dir

echo "moving SHAPE sources"
rsync -au --copy-links --progress $shape_src_svn/* $shape_src
echo "moving SHINE sources"
rsync -au --copy-links --progress $shine_src_gitclone/* $shine_src
echo "moving DB"
rsync -au --copy-links --progress $shine_db_svn/* $db_dir

cp $shape_src/shape.rc.example ~/.shaperc

sed -ie "s~base = %(home)s/NA61Soft/Shape_install~base=$shape_install~" ~/.shaperc
sed -ie "s~build = %(home)s/NA61Soft/Shape_build~build=$shape_build~" ~/.shaperc
sed -ie "s~jobs = 1~jobs=16~" ~/.shaperc

# https://wiki.gsi.de/foswiki/bin/view/Linux/EnvironmentModules
# https://wiki.gsi.de/foswiki/bin/view/Linux/SoftwareInCvmfs
module use /cvmfs/it.gsi.de/modulefiles/
module load compiler/gcc/4.8.4

# copy sources for externals from my directory
# TODO download during previous step and 
mkdir -p $shape_build/distfiles/
cp /u/klochkov/na61/distfiles/* $shape_build/distfiles/
sed -ie "s~version = 1.0.5~version = 1.0.6~" $shape_src/config/bzip2.rc
sed -ie "s~makeArgs = PREFIX=%(prefix)s~makeArgs = CFLAGS="-fPIC" CXXFLAGS="-fPIC" PREFIX=%(prefix)s~" $shape_src/config/bzip2.rc

eval $($shape_src/shape sh)
# install externals
# NOTE: if an error during install, to re-install: remove package from ./shape_build and ./shape_install

$shape_src/shape install bzip2

# load enviroment variables
eval $($shape_src/shape sh)
export LIBRARY_PATH=$shape_install/External/bzip2/1.0.6/lib
export CPLUS_INCLUDE_PATH=$shape_install/External/bzip2/1.0.6/include
export C_INCLUDE_PATH=/usr/include/x86_64-linux-gnu
export CPLUS_INCLUDE_PATH=/usr/include/x86_64-linux-gnu

$shape_src/shape install boost
$shape_src/shape install externals  
$shape_src/shape install hepmc
$shape_src/shape install dbdownload

if [ "$isgeant4" == "ON" ]; then
    $shape_src/shape install geant4
fi

echo *** Setting SHINECALIBHOME ***

# svn co svn+ssh://svn.cern.ch/reps/na61db/trunk $db_dir/v2 
shinecalibhome_local=$db_dir
sed -i.bak "s~export SHINECALIBHOME=/afs/cern.ch/na61/ReleasesDB~export SHINECALIBHOME=${db_dir}~g" $shine_src/Tools/Scripts/env/lxplus_64bit_slc6.sh

source $shine_src/Tools/Scripts/env/lxplus_64bit_slc6.sh

if [[ $shinecalibhome_local != $SHINECALIBHOME ]]; then
    echo $shinecalibhome_local != $SHINECALIBHOME! Exiting...
    exit 1
fi 

export SHINECALIBHOME=$shinecalibhome_local


# don't need it for SHINE installation
module unload compiler/gcc/4.8.4

cd $shine_build

$shape_src/shape sh

eval $($shape_src/shape sh)
cmake -DSHINE_DEBUG_BUILD=OFF -DSHINE_GEANT4_ENABLED=$isgeant4 -DPRODUCTION=$prodprefix -DCMAKE_INSTALL_PREFIX=$shine_install $shine_src
make -j16 install

# to start using SHINE:
# eval $($shape_src/shape sh)
# eval $($shine_install/bin/shine-offline-config --env-sh)
