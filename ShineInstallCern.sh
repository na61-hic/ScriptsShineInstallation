#!/bin/bash

LEGACY=$1

bit=64
prodprefix=OFF

if [ "$LEGACY" == "LEGACY" ]; then
    bit=32
    prodprefix=ON
fi

echo Legacy support: $prodprefix

USERNAME=$USER
first_username_letter=${USERNAME:0:1}
shine_dir=/afs/cern.ch/work/$first_username_letter/$USERNAME/public/shine/
shine_src=$shine_dir/shine_src
shine_install=$shine_dir/shine_install
shine_build=$shine_dir/shine_build

echo $shine_dir
mkdir $shine_dir
echo $shine_src
mkdir $shine_src
echo $shine_install
mkdir $shine_install
echo $shine_build
mkdir $shine_build
git clone https://:@gitlab.cern.ch:8443/na61-software/framework/Shine.git $shine_src

source $shine_src/Tools/Scripts/env/lxplus_${bit}bit_slc6.sh
cd $shine_build
cmake -DPRODUCTION=$prodprefix -DCMAKE_INSTALL_PREFIX=$shine_install $shine_src
make -j8 install
