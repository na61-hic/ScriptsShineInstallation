#!/bin/bash

shine_dir=/u/$USER/shine
shape_dir=/u/$USER/shape

shape_src=$shape_dir/shape_src 
shape_install=/lustre/nyx/cbm/users/$USER/shape_install 
shape_build=$shape_dir/shape_build 
shape_src_svn=/u/$USER/shape_src_svn

shine_src=$shine_dir/shine_src
shine_install=/lustre/nyx/cbm/users/$USER/shine_install
shine_build=$shine_dir/shine_build
shine_src_gitclone=/u/$USER/shine_src_gitclone

shine_db_svn=/u/$USER/shine_db_svn
db_dir=$shape_install/db


if [[ $1 == "sh" ]]; then

echo export shine_dir=$shine_dir
echo export shape_dir=$shape_dir

echo export shape_src=$shape_src
echo export shape_install=$shape_install
echo export shape_build=$shape_build
echo export shape_src_svn=$shape_src_svn

echo export shine_src=$shine_src
echo export shine_install=$shine_install
echo export shine_build=$shine_build
echo export shine_src_gitclone=$shine_src_gitclone

echo export shine_db_svn=$shine_db_svn
echo export db_dir=$db_dir

exit 0
fi

echo Fetching Shape...
echo -n "Enter svn.cern.ch username: " 
read SVNUSERNAME
svn checkout svn+ssh://$SVNUSERNAME@svn.cern.ch/reps/na61soft/trunk/Shape $shape_src_svn

echo Fetching NA61DB...
svn checkout svn+ssh://$SVNUSERNAME@svn.cern.ch/reps/na61db/trunk $shine_db_svn/v2


echo Fetching Shine...
echo -n "Enter gitlab.cern.ch username: " 
read GITLABUSERNAME
git clone https://$GITLABUSERNAME@gitlab.cern.ch/na61-software/framework/Shine.git $shine_src_gitclone




